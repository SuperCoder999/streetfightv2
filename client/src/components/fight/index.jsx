import React from 'react';

import { getFighters } from '../../services/domainRequest/fightersRequest';
import { createFight, addToFightLog } from "../../services/domainRequest/fightRequest";
import NewFighter from '../newFighter';
import Fighter from '../fighter';
import { Button } from '@material-ui/core';

import './fight.css'

class Fight extends React.Component {
    state = {
        fighters: [],
        fighter1: null,
        fighter2: null
    };

    async componentDidMount() {
        const fighters = await getFighters();
        if(fighters && !fighters.error) {
            this.setState({ fighters });
        }
    }

    onFightStart = async () => {
        const pressed = new Set();
        const { fighter1: fighter1Temp, fighter2: fighter2Temp } = this.state;

        if (!fighter1Temp && !fighter2Temp) return;

        const fighter1 = Object.create(fighter1Temp);
        const fighter2 = Object.create(fighter2Temp);

        const newFight = await createFight(fighter1.name, fighter2.name);

        const f1Health = document.getElementById(fighter1.name + "H");
        const f2Health = document.getElementById(fighter2.name + "H");

        f1Health.innerHTML = "Health: " + fighter1Temp.health;
        f2Health.innerHTML = "Health: " + fighter2Temp.health;

        fighter1.lastCritical = -10;
        fighter2.lastCritical = -10;

        fighter1.block = false;
        fighter2.block = false;

        function getPower(attacker) {
            return attacker.power * (Math.floor(Math.random() * 2) + 1);
        }

        function getBlock(defender) {
            return defender.defense * (Math.floor(Math.random() * 2) + 1);
        }

        function getAttack(attacker, defender) {
            return Math.max(getPower(attacker) - getBlock(defender), 0);
        }

        function getCritical(attacker) {
            return attacker.power * 2;
        }

        function attack(pressed) {
            if (pressed.has("KeyA") && !fighter1.block && !fighter2.block) {
                fighter2.health -= getAttack(fighter1, fighter2);
                addToFightLog(newFight, "Fighter1: Attack");
            }

            if (pressed.has("KeyJ") && !fighter1.block && !fighter2.block) {
                fighter1.health -= getAttack(fighter2, fighter1);
                addToFightLog(newFight, "Fighter2: Attack");
            }
        }

        function block(pressed) {
            if (pressed.has("KeyD")) {
                fighter1.block = true;
                addToFightLog(newFight, "Fighter1: Block");
            } else {
                fighter1.block = false;
            }

            if (pressed.has("KeyL")) {
                fighter2.block = true;
                addToFightLog(newFight, "Fighter2: Block");
            } else {
                fighter2.block = false;
            }
        }

        function critical(pressed) {
            if (pressed.has("KeyQ") && pressed.has("KeyW") && pressed.has("KeyE") && !fighter1.block && (performance.now() / 1000 - fighter1.lastCritical) >= 10) {
                fighter2.health -= getCritical(fighter1);
                fighter1.lastCritical = performance.now() / 1000;
                addToFightLog(newFight, "Fighter1: Critical");
            }

            if (pressed.has("KeyU") && pressed.has("KeyI") && pressed.has("KeyO") && !fighter2.block && (performance.now() / 1000 - fighter2.lastCritical) >= 10) {
                fighter1.health -= getCritical(fighter2);
                fighter2.lastCritical = performance.now() / 1000;
                addToFightLog(newFight, "Fighter2: Critical");
            }
        }

        function winner() {
            let haveWinner = false;

            if (fighter1.health <= 0) {
                setTimeout(() => alert(fighter2.name + " wins!"), 100);
                haveWinner = true;
                addToFightLog(newFight, "Fighter2: Win");
            } else if (fighter2.health <= 0) {
                setTimeout(() => alert(fighter1.name + " wins!"), 100);
                haveWinner = true;
                addToFightLog(newFight, "Fighter1: Win");
            }

            if (haveWinner) {
                document.body.onkeydown = () => {};
                document.body.onkeyup = () => {};
                f1Health.innerHTML = "Health: " + fighter1Temp.health;
                f2Health.innerHTML = "Health: " + fighter2Temp.health;
            }
        }

        document.body.onkeydown = (event) => {
            pressed.add(event.code);
            // Check...
            attack(pressed);
            block(pressed);
            critical(pressed);
            winner();
            //
            f1Health.innerHTML = "Health: " + fighter1.health;
            f2Health.innerHTML = "Health: " + fighter2.health;
        }

        document.body.onkeyup = (event) => {
            pressed.delete(event.code);
        }
    }

    onCreate = (fighter) => {
        this.setState({ fighters: [...this.state.fighters, fighter] });
    }

    onFighter1Select = (fighter1) => {
        this.setState({fighter1 });
    }

    onFighter2Select = (fighter2) => {
        this.setState({ fighter2 });
    }

    getFighter1List = () => {
        const { fighter2, fighters } = this.state;
        if(!fighter2) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter2.id);
    }

    getFighter2List = () => {
        const { fighter1, fighters } = this.state;
        if(!fighter1) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter1.id);
    }

    render() {
        const  { fighter1, fighter2 } = this.state;
        return (
            <div id="wrapper">
                <NewFighter onCreated={this.onCreate} />
                <div id="figh-wrapper">
                    <Fighter selectedFighter={fighter1} onFighterSelect={this.onFighter1Select} fightersList={this.getFighter1List() || []} />
                    <div className="btn-wrapper">
                        <Button onClick={this.onFightStart} variant="contained" color="primary">Start Fight</Button>
                    </div>
                    <Fighter selectedFighter={fighter2} onFighterSelect={this.onFighter2Select} fightersList={this.getFighter2List() || []} />
                </div>
            </div>
        );
    }
}

export default Fight;