import { post, put } from "../requestHelper";

export const createFight = async (f1, f2) => {
    return await post("fights", {fighter1: f1, fighter2: f2});
}

export const addToFightLog = async (fight, string) => {
    if (!fight.log) fight.log = [];
    fight.log.push(string);
    return await put("fights", fight.id, {log: [...fight.log]});
}