const { fight } = require("../models/fight");

const createFightValid = (req, res, next) => {
    for (const key of Object.keys(fight)) {
        if (!Object.keys(req.body).includes(key) && key !== "id" && key !== "log") {
            res.err = {
                status: 400,
                object: {
                    error: true,
                    message: `Error 400: No field ${key} in given data`
                }
            };

            return next();
        }
    }

    for (const key of Object.keys(req.body)) {
        if (!Object.keys(user).includes(key) || key === "id") {
            res.err = {
                status: 400,
                object: {
                    error: true,
                    message: `Error 400: Unexpected field ${key}`
                }
            }

            return next();
        }
    }

    next();
}

const updateFightValid = (req, res, next) => {
    for (const key of Object.keys(req.body)) {
        if (!Object.keys(user).includes(key) || key === "id") {
            res.err = {
                status: 400,
                object: {
                    error: true,
                    message: `Error 400: Unexpected field ${key}`
                }
            }

            return next();
        }
    }
    
    next();
}

exports.createFightValid = createFightValid;
exports.updateFightValid = updateFightValid;