const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    for (const key of Object.keys(fighter)) {
        if (!Object.keys(req.body).includes(key) && key !== "id") {
            res.err = {
                status: 400,
                object: {
                    error: true,
                    message: `Error 400: No field ${key} in given data`
                }
            };

            return next();
        }
    }

    for (const key of Object.keys(req.body)) {
        if (!Object.keys(fighter).includes(key) || key === "id") {
            res.err = {
                status: 400,
                object: {
                    error: true,
                    message: `Error 400: Unexpected field ${key}`
                }
            }

            return next();
        }
    }

    const power = req.body.power;

    if (!((typeof power) === "number" && power <= 100 && power >= 0)) {
        res.err = {
            status: 400,
            object: {
                error: true,
                message: `Error 400: Power should be 0 <= power <= 100`
            }
        };

        return next();
    }

    const health = req.body.health;

    if (!((typeof health) === "number" && health <= 100 && health >= 0)) {
        res.err = {
            status: 400,
            object: {
                error: true,
                message: `Error 400: Health should be 0 <= health <= 100`
            }
        };

        return next();
    }

    const defense = req.body.defense;

    if (!((typeof defense) === "number" && defense <= 10 && defense >= 0)) {
        res.err = {
            status: 400,
            object: {
                error: true,
                message: `Error 400: Defense should be 0 <= defense <= 10`
            }
        };

        return next();
    }

    next();
}

const updateFighterValid = (req, res, next) => {
    for (const key of Object.keys(req.body)) {
        if (!Object.keys(fighter).includes(key) || key === "id") {
            res.err = {
                status: 400,
                object: {
                    error: true,
                    message: `Error 400: Unexpected field ${key}`
                }
            }

            return next();
        }
    }

    const power = req.body.power;

    if (power && !((typeof power) === "number" && power <= 100 && power >= 0)) {
        res.err = {
            status: 400,
            object: {
                error: true,
                message: `Error 400: Power should be 0 <= power <= 100`
            }
        };

        return next();
    }

    const health = req.body.health;

    if (health && !((typeof health) === "number" && health <= 100 && health >= 0)) {
        res.err = {
            status: 400,
            object: {
                error: true,
                message: `Error 400: Health should be 0 <= health <= 100`
            }
        };

        return next();
    }

    const defense = req.body.defense;

    if (defense && !((typeof defense) === "number" && defense <= 10 && defense >= 0)) {
        res.err = {
            status: 400,
            object: {
                error: true,
                message: `Error 400: Defense should be 0 <= defense <= 10`
            }
        };

        return next();
    }

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;