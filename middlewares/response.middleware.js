const responseMiddleware = (req, res, next) => {
    if (res.err) {
        res.status(res.err.status || 400).json({ error: true, ...res.err.object });
    } else {
        res.status(200).json(res.data || {
            error: false,
            message: "Success"
        });
    }

    next();
}

exports.responseMiddleware = responseMiddleware;