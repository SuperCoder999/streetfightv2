const { user } = require('../models/user');
const { getAllUsers } = require("../services/helpers/requestHelper");

const createUserValid = (req, res, next) => {
    for (const key of Object.keys(user)) {
        if (!Object.keys(req.body).includes(key) && key !== "id") {
            res.err = {
                status: 400,
                object: {
                    error: true,
                    message: `Error 400: No field ${key} in given data`
                }
            };

            return next();
        }
    }

    for (const key of Object.keys(req.body)) {
        if (!Object.keys(user).includes(key) || key === "id") {
            res.err = {
                status: 400,
                object: {
                    error: true,
                    message: `Error 400: Unexpected field ${key}`
                }
            }

            return next();
        }
    }

    const email = req.body.email;

    if (!(~email.indexOf(".") && ~email.indexOf("@") && email.indexOf("@") < email.lastIndexOf(".") && email.split("@")[1] === "gmail.com")) {
        res.err = {
            status: 400,
            object: {
                error: true,
                message: `Error 400: Invalid email address. <address>@gmail.com expected`
            }
        };

        return next();
    }

    const telephone = req.body.phoneNumber;

    if (!/\+380[0-9]{9,9}/.test(telephone)) {
        res.err = {
            status: 400,
            object: {
                error: true,
                message: `Error 400: Phone number should be +380xxxxxxxxx`
            }
        };

        return next();
    }

    const password = req.body.password;

    if (password.length < 3) {
        res.err = {
            status: 400,
            object: {
                error: true,
                message: `Error 400: Password should be 3+ symbols`
            }
        };

        return next();
    }

    getAllUsers().then(body => {
        const emails = body.map(user => user.email);
        const telephones = body.map(user => user.phoneNumber);

        if (emails.includes(email)) {
            res.err = {
                status: 400,
                object: {
                    error: true,
                    message: "Error 400: Email is non-unique"
                }
            }

            return next();
        } else if (telephones.includes(telephone)) {
            res.err = {
                status: 400,
                object: {
                    error: true,
                    message: "Error 400: Phone number is non-unique"
                }
            }

            return next();
        }

        next();
    });
}

const updateUserValid = (req, res, next) => {
    for (const key of Object.keys(req.body)) {
        if (!Object.keys(user).includes(key) || key === "id") {
            res.err = {
                status: 400,
                object: {
                    error: true,
                    message: `Error 400: Unexpected field ${key}`
                }
            }

            return next();
        }
    }

    const email = req.body.email;

    if (email && !(~email.indexOf(".") && ~email.indexOf("@") && email.indexOf("@") < email.lastIndexOf(".") && email.split("@")[1] === "gmail.com")) {
        res.err = {
            status: 400,
            object: {
                error: true,
                message: `Error 400: Invalid email address. <address>@gmail.com expected`
            }
        };

        return next();
    }

    const telephone = req.body.phoneNumber;

    if (telephone && !/\+380[0-9]{9,9}/.test(telephone)) {
        res.err = {
            status: 400,
            object: {
                error: true,
                message: `Error 400: Phone number should be +380xxxxxxxxx`
            }
        };

        return next();
    }

    const password = req.body.password;

    if (password && password.length < 3) {
        res.err = {
            status: 400,
            object: {
                error: true,
                message: `Error 400: Password should be 3+ symbols`
            }
        };

        return next();
    }

    getAllUsers().then(body => {
        const emails = body.map(user => user.email);
        const telephones = body.map(user => user.phoneNumber);

        if (emails.includes(email)) {
            res.err = {
                status: 400,
                object: {
                    error: true,
                    message: "Error 400: Email is non-unique"
                }
            }

            return next();
        } else if (telephones.includes(telephone)) {
            res.err = {
                status: 400,
                object: {
                    error: true,
                    message: "Error 400: Phone number is non-unique"
                }
            }

            return next();
        }

        next();
    });

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;