const { Router } = require('express');
const FightService = require('../services/fightService');
const { createFightValid, updateFightValid } = require('../middlewares/fight.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

router.get("/", (req, res, next) => {
    res.data = FightService.all();
    next();
}, responseMiddleware);

router.get("/:id", (req, res, next) => {
    const item = FightService.search({ id: req.params.id });

    if (item) {
        res.data = item;
    } else {
        res.err = {
            status: 404,
            object: {
                error: true,
                message: "Fight not found"
            }
        }
    }

    next();
}, responseMiddleware);

router.post("/", createFightValid, (req, res, next) => {
    if (!res.err) {
        res.data = FightService.create(req.body);
    }

    next();
}, responseMiddleware);

router.put("/:id", updateFightValid, (req, res, next) => {
    if (!res.err) {
        try {
            res.data = FightService.update(req.params.id, req.body);
        } catch (e) {
            res.err = e;
        }
    }

    next();
}, responseMiddleware);

router.delete("/:id", (req, res, next) => {
    try {
        res.data = FightService.delete(req.params.id);
    } catch (e) {
        res.err = e;
    }
    
    next();
}, responseMiddleware);

module.exports = router;