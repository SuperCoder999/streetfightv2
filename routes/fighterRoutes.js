const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get("/", (req, res, next) => {
    res.data = FighterService.all();
    next();
}, responseMiddleware);

router.get("/:id", (req, res, next) => {
    const item = FighterService.search({ id: req.params.id });

    if (item) {
        res.data = item;
    } else {
        res.err = {
            status: 404,
            object: {
                error: true,
                message: "Fighter not found"
            }
        }
    }

    next();
}, responseMiddleware);

router.post("/", createFighterValid, (req, res, next) => {
    if (!res.err) {
        res.data = FighterService.create(req.body);
    }

    next();
}, responseMiddleware);

router.put("/:id", updateFighterValid, (req, res, next) => {
    if (!res.err) {
        try {
            res.data = FighterService.update(req.params.id, req.body);
        } catch (e) {
            res.err = e;
        }
    }

    next();
}, responseMiddleware);

router.delete("/:id", (req, res, next) => {
    try {
        res.data = FighterService.delete(req.params.id);
    } catch (e) {
        res.err = e;
    }
    
    next();
}, responseMiddleware);

module.exports = router;