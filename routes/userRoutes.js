const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get("/", (req, res, next) => {
    res.data = UserService.all();
    next();
}, responseMiddleware);

router.get("/:id", (req, res, next) => {
    const item = UserService.search({ id: req.params.id });

    if (item) {
        res.data = item;
    } else {
        res.err = {
            status: 404,
            object: {
                error: true,
                message: "User not found"
            }
        }
    }

    next();
}, responseMiddleware);

router.post("/", createUserValid, (req, res, next) => {
    if (!res.err) {
        res.data = UserService.create(req.body);
    }

    next();
}, responseMiddleware);

router.put("/:id", updateUserValid, (req, res, next) => {
    if (!res.err) {
        try {
            res.data = UserService.update(req.params.id, req.body);
        } catch (e) {
            res.err = e;
        }
    }
    
    next();
}, responseMiddleware);

router.delete("/:id", (req, res, next) => {
    try {
        res.data = UserService.delete(req.params.id);
    } catch (e) {
        res.err = e;
    }

    next();
}, responseMiddleware);

module.exports = router;