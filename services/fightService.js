const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
    all() {
        return FightRepository.getAll();
    }

    search(search) {
        const item = FightRepository.search(search);

        if (!item) {
            return null;
        }

        return item;
    }

    create(data) {
        return FightRepository.create(data);
    }

    update(id, data) {
        if (this.search({ id: id }) !== null) {
            return FightRepository.update(id, data);
        } else {
            throw {
                status: 404,
                object: {
                    error: true,
                    message: `Fight with id ${id} not found`
                }
            }
        }
    }

    delete(id) {
        if (this.search({ id: id })) {
            return FightRepository.delete(id);
        } else {
            throw {
                status: 404,
                object: {
                    error: true,
                    message: `Fight with id ${id} not found`
                }
            }
        }
    }
}

module.exports = new FightersService();