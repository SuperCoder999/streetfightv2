const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    all() {
        return FighterRepository.getAll();
    }

    search(search) {
        const item = FighterRepository.getOne(search);

        if (!item) {
            return null;
        }

        return item;
    }

    update(id, data) {
        if (this.search({ id: id }) !== null) {
            return FighterRepository.update(id, data);
        } else {
            throw {
                status: 404,
                object: {
                    error: true,
                    message: `Fighter with id ${id} not found`
                }
            }
        }
    }

    create(data) {
        return FighterRepository.create(data);
    }

    delete(id) {
        if (this.search({ id: id }) !== null) {
            return FighterRepository.delete(id);
        } else {
            throw {
                status: 404,
                object: {
                    error: true,
                    message: `Fighter with id ${id} not found`
                }
            }
        }
    }
}

module.exports = new FighterService();