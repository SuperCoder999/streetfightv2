const http = require("http");

async function getAllUsers() {
    return new Promise(resolve => {
        http.get({
            host: "127.0.0.1",
            path: "/api/users",
            method: "GET",
            port: process.env.PORT || 3050
        }, res => {
            let body = "";
            
            res.on("data", chunk => {
                body += chunk;
            });

            res.on("end", () => {
                resolve(JSON.parse(body));
            });
        });
    });
}

exports.getAllUsers = getAllUsers;