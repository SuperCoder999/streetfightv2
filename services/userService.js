const { UserRepository } = require('../repositories/userRepository');

class UserService {
    all() {
        return UserRepository.getAll();
    }

    update(id, data) {
        if (this.search({ id: id }) !== null) {
            return UserRepository.update(id, data);
        } else {
            throw {
                status: 404,
                object: {
                    error: true,
                    message: `User with id ${id} not found`
                }
            }
        }
    }

    delete(id) {
        if (this.search({ id: id }) !== null) {
            return UserRepository.delete(id);
        } else {
            throw {
                status: 404,
                object: {
                    error: true,
                    message: `User with id ${id} not found`
                }
            }
        }
    }

    create(data) {
        return UserRepository.create(data);
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();